#N vpatcher
#X obj 180 150 jit.qt.grab;
#X obj 180 180 jit.matrix;
#X obj 180 210 jit.change;
#X obj 180 240 select;
#X obj 180 270 vst~ synth;
#X connect 0 0 1 0;
#X connect 1 0 2 0;
#X connect 2 0 3 0;
#X connect 3 0 4 0;
#X connect 3 1 4 0;
