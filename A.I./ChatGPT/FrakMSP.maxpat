// Import the jitter library

#include "jit.common.h"

// Create objects to capture and hold video data

jit.qt.grab grabber;
jit.matrix matrix;

// Create a change detection object

jit.change change;

// Route video data to synthesizer only when a change is detected

select sel;

// Load and process synthesizer

vst~ synth;

// Connect objects

grabber.output_matrix(matrix);
change.matrix(matrix);
sel.input_a(matrix);
sel.input_b(change);
sel.output(synth);
