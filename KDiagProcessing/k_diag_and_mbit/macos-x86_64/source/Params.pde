interface ISimpleAutomaton {
  void next();
  void draw();
  boolean isPlaying();
}

PFont paramFont;

abstract class BaseParam extends TimeSlider {
  float min, max;
  String title;
  boolean playing=true;
  float a=0;
  float da=0.001;
  int c, dc;
  boolean isPlaying() { 
    return playing;
  }
  BaseParam(String tit, float min, float max) {
    super(0, 0, min, max);
    this.min = min;
    this.max = max;
    this.title = tit;
    da = random(1)/1000;
    c = 0;
    dc = (int) random(10);
  }

  void setCorner(int xx, int yy) {
    x = xx;
    y = yy;
  }

  void next() {
    update();
    if (moving) {
      sendData();
    }
  }

  abstract void sendData();
  abstract String getPath();
  abstract String pp();

  void draw() {
    if (moving) {
      draw(color(255, 100, 100, 50));
    } else {
      draw(color(100, 100, 255, 50));
    }
    textFont(paramFont);
    stroke(255);
    fill(255);
    text(getPath(), x, y);
  }
}

class OSCParam extends BaseParam implements OSCObserver, ISimpleAutomaton {

  String type;

  String path;
  OscP5 oscP5;
  NetAddress myRemoteLocation; 

  OSCParam(int remotePort, String tit, String typ, float min, float max, String pName) {
    super(tit, min, max);

    type = typ;

    this.path = "/"+pName;

    oscP5 = new OscP5(this, 9003);
    myRemoteLocation = new NetAddress("127.0.0.1", remotePort);
  }



  String pp() {
    return title+ ": ("+type+") " + min + "-" + max + "  " + path;
  }

  NetAddress getRemoteLocation() {
    return myRemoteLocation;
  }

  void sendData() { 
    sendOSC();
  }
  String getPath() { 
    return this.path;
  }

  void sendOSC() {
    OscMessage m = new OscMessage(path);
    float v = getValue();      
    if (type=="int") {
      m.add((int)v);
    } else {
      m.add(v);
    }      
    oscP5.send(m, myRemoteLocation);
  }
}


class MidiParam extends BaseParam implements  ISimpleAutomaton {


  int chan, ccVal;

  MidiProxy midiProxy;

  boolean playing=true;
  float a=0;
  float da=0.001;
  int c, dc;

  MidiParam(int chan, int ccval, String tit, float min, float max, MidiProxy proxy) {
    super(tit, min, max);

    this.chan = chan;
    this.ccVal = ccval;
    this.title = tit;

    this.min = min;
    this.max = max;

    this.midiProxy = proxy;

    da = random(1)/1000;
    c = 0;
    dc = (int) random(10);
  }

  void sendData() {
    midiProxy.sendCC(chan, ccVal, (int)getValue());
  }

  String getPath() { 
    return ""+ccVal;
  }

  String pp() { 
    return "CC " + chan + ", " + ccVal + ", currently: " + (int)getValue();
  }
}


ArrayList<BaseParam> paramList = new ArrayList<BaseParam>();
void setupOSCParams() {
  paramList.add(new OSCParam(5001, "harm_(LFO)_time_(envelope_duration_in_ms)", "int", 0, 1024, "harm_time"));
  paramList.add(new OSCParam(5001, "harm_(LFO)_depth_(intensity)", "float", 0, 1024, "harm_depth"));
  paramList.add(new OSCParam(5001, "harm_(LFO)_freq_", "float", 0, 1024, "harm_freq"));
  paramList.add(new OSCParam(5001, "harm_(LFO)_range", "float", 0, 1024, "harm_range"));
  paramList.add(new OSCParam(5001, "mod_(LFO)_time_(envelope_in_ms)", "int", 0, 1024, "mod_time"));
  paramList.add(new OSCParam(5001, "mod_(LFO)_depth_(intensity)", "float", 0, 1024, "mod_depth"));
  paramList.add(new OSCParam(5001, "mod_(LFO)_freq_", "float", 0, 1024, "mod_freq"));
  paramList.add(new OSCParam(5001, "mod_(LFO)_range", "float", 0, 1024, "mod_range"));
  paramList.add(new OSCParam(5001, "shift_(LFO)_time_(envelope_in_ms)", "int", 0, 1024, "shift_time"));
  paramList.add(new OSCParam(5001, "shift_(LFO)_depth_(intensity)", "float", 0, 1024, "shift_depth"));
  paramList.add(new OSCParam(5001, "shift_(LFO)_freq_", "float", 0, 1024, "shift_freq"));
  paramList.add(new OSCParam(5001, "shift_(LFO)_range", "float", 0, 1024, "shift_range"));
  paramList.add(new OSCParam(5001, "amp_(LFO)_time_(envelope_in_ms)", "int", 1, 127, "amp_time"));
  paramList.add(new OSCParam(5001, "amp_(LFO)_depth_(intensity)", "float", 0, 2, "amp_depth"));
  paramList.add(new OSCParam(5001, "amp_(LFO)_freq_", "float", 0, 1024, "amp_freq"));
  paramList.add(new OSCParam(5001, "amp_(LFO)_range", "float", 0, 1024, "amp_range"));
  paramList.add(new OSCParam(5001, "vib_(LFO)_time_(envelope_in_ms)", "int", 1, 127, "vib_time"));
  paramList.add(new OSCParam(5001, "vib_(LFO)_depth_(intensity)", "float", 0, 1024, "vib_depth"));
  paramList.add(new OSCParam(5001, "vib_(LFO)_freq_", "float", 0, 1024, "vib_freq"));
  paramList.add(new OSCParam(5001, "vib_(LFO)_range", "float", 0, 1024, "vib_range"));
  paramList.add(new OSCParam(5001, "reverb_size", "int", 0, 127, "rev_size"));
  paramList.add(new OSCParam(5001, "reverb_decay", "int", 0, 127, "rev_decay"));
  paramList.add(new OSCParam(5001, "reverb_damp", "int", 0, 127, "rev_damp"));
  paramList.add(new OSCParam(5001, "reverb_diff", "int", 0, 127, "rev_diff"));

  int xx=10; 
  int yy=10;
  for (BaseParam p : paramList) {
    p.setCorner(xx, yy);
    println(p.pp());
    xx=(xx+(int)p.w+5)%550;
    if (xx<p.w+10) {
      yy=(yy+(int)p.h+5);
      xx=10;
    }
  }
}

void setupMidiParams(MidiProxy midiProxy) {
  paramList.add(new MidiParam(1, 50, "harm_(LFO)_time_(envelope_duration_in_ms)", 1, 127, midiProxy));
  paramList.add(new MidiParam(1, 51, "harm_(LFO)_depth_(intensity)", 1, 127, midiProxy));
  paramList.add(new MidiParam(1, 52, "harm_(LFO)_freq_", 1, 127, midiProxy));
  paramList.add(new MidiParam(1, 53, "harm_(LFO)_range", 1, 127, midiProxy));
  paramList.add(new MidiParam(1, 54, "mod_(LFO)_time_(envelope_in_ms)", 1, 127, midiProxy));
  paramList.add(new MidiParam(1, 55, "mod_(LFO)_depth_(intensity)", 1, 127, midiProxy));
  paramList.add(new MidiParam(1, 56, "mod_(LFO)_freq_", 1, 127, midiProxy));
  paramList.add(new MidiParam(1, 57, "mod_(LFO)_range", 1, 127, midiProxy));
  paramList.add(new MidiParam(1, 58, "shift_(LFO)_time_(envelope_in_ms)", 1, 127, midiProxy));
  paramList.add(new MidiParam(1, 59, "shift_(LFO)_depth_(intensity)", 1, 127, midiProxy));
  paramList.add(new MidiParam(1, 60, "shift_(LFO)_freq_", 1, 127, midiProxy));
  paramList.add(new MidiParam(1, 61, "shift_(LFO)_range", 1, 127, midiProxy));
  paramList.add(new MidiParam(1, 62, "amp_(LFO)_time_(envelope_in_ms)", 1, 127, midiProxy));
  paramList.add(new MidiParam(1, 63, "amp_(LFO)_depth_(intensity)", 1, 127, midiProxy));
  paramList.add(new MidiParam(1, 64, "amp_(LFO)_freq_", 1, 127, midiProxy));
  paramList.add(new MidiParam(1, 65, "amp_(LFO)_range", 1, 127, midiProxy));
  paramList.add(new MidiParam(1, 66, "vib_(LFO)_time_(envelope_in_ms)", 1, 127, midiProxy));
  paramList.add(new MidiParam(1, 67, "vib_(LFO)_depth_(intensity)", 1, 127, midiProxy));
  paramList.add(new MidiParam(1, 68, "vib_(LFO)_freq_", 1, 127, midiProxy));
  paramList.add(new MidiParam(1, 69, "vib_(LFO)_range", 1, 127, midiProxy));
  paramList.add(new MidiParam(1, 70, "reverb_size", 1, 127, midiProxy));
  paramList.add(new MidiParam(1, 71, "reverb_decay", 1, 127, midiProxy));
  paramList.add(new MidiParam(1, 72, "reverb_damp", 1, 127, midiProxy));
  paramList.add(new MidiParam(1, 73, "reverb_diff", 1, 127, midiProxy));



  int xx=10; 
  int yy=10;
  for (BaseParam p : paramList) {
    p.setCorner(xx, yy);
    println(p.pp());
    xx=(xx+(int)p.w+5)%550;
    if (xx<p.w+10) {
      yy=(yy+(int)p.h+5);
      xx=10;
    }
  }
}
