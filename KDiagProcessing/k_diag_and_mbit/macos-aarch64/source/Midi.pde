
import themidibus.*; //Import the library
MidiBus globalMidiBus;


abstract class MidiSender implements Runnable {
  MidiBus bus; 
  abstract public void run();
}

class Throttler {
   int[] timers = new int[16];
   Throttler() {
     for (int i=0;i<16;i++) { timers[i]=0; }
   }
   
   void step() {
    for (int i=0;i<16;i++) { 
      timers[i]=timers[i]-1;
    }
   }
   
   boolean test(int chan) {
     return (timers[chan-1] < 1);
   }
   
   void set(int chan, int val) {
     timers[chan-1]=val;
   }
}

public class PlayableNote extends MidiSender {
  int note;
  int value;
  int channel;
  int duration;
  
  PlayableNote(int c, int n, int v, int d, MidiBus b) {
    channel = c;
    note = n;
    value = v;
    duration = d;
    bus = b;
  }

  public void run() {
    bus.sendNoteOn(channel-1, note, value);
    try {
      Thread.sleep(duration);
    } catch (InterruptedException e) {
      bus.sendNoteOff(channel, note, value);
    }
    bus.sendNoteOff(channel-1, note, value);
  }  
}


 

class CCStream extends MidiSender {
  int channel, val, vel;
  CCStream(int c, int v1, int v2, MidiBus b) {
    channel = c;
    val = v1;
    vel = v2;
    bus = b;
  }
  
   public void run() {
     for (int i=0;i<1;i++) {
        bus.sendControllerChange(channel, val, vel) ;
        try {
          Thread.sleep(80);
        } catch (InterruptedException e) {}
     }
   }
}



class MidiProxy implements IPlayerProxy {
  MidiBus bus;
  
  MidiProxy(MidiBus b) {
    bus = b; 
  }
  
  void sendCC(int channel, int val, int vel) {
    //println("Creating a new thread");
    Thread t = new Thread(new CCStream(channel,val,vel, bus));
    t.start();
  }
 
  void sendNote(int channel,int pitch,int value) {
    //if (throttler.test(channel)) {
    //  throttler.set(channel,5);
      Thread t = new Thread(new PlayableNote(channel,pitch,value, 100, bus));
      t.start();
    //}
  }
  
}

class DummyProxy implements IPlayerProxy {
 void sendCC(int channel, int val, int vel) { 
   println("DummProxy sendCC");
 }
 void sendNote(int channel, int pitch, int value) {
   println("DummyProxy::sendNote");
 }
}

public static int frequencyToMidi(double frequency) {
    double midiDouble = 69 + 12 * Math.log(frequency / 440) / Math.log(2);
    return (int)Math.round(midiDouble);
}

class MidiObservingInstrument extends MidiProxy implements ObservingInstrument {
  
  MidiObservingInstrument(MidiBus mbus) {
    super(mbus);
  }
  void pingMsg(String s) {
    println(s);
  }
  void playNote(float freq) {
    playNote(freq,80);
  }
  
  void playNote(float freq, float vel) {
     println("MidiObservingInstrument sending " + freq + " , " + vel);
     sendNote(1,frequencyToMidi(freq),int(vel));
  }  
}

class MidiFactory {
    MidiBus myBus;
    
    MidiFactory() {
       MidiBus.list();
       myBus = new MidiBus(this, -1, "Bus 1");
    }
    
    MidiObservingInstrument makeMOI() {
      return new MidiObservingInstrument(myBus);
    }
    
    MidiProxy getAProxy() {
      return new MidiProxy(myBus);
    }
}
