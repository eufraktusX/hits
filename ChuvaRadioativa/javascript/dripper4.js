// inlets and outlets
inlets = 1;
outlets = 2;

// global variables
var newParticle, flatten, maybe_merge, maybe_split, merge, report, run, shuffle, split, split_nrg, the_system, update_system;


newParticle = function(t,e) {
   return {type: t, energy: e};
}

split_nrg = function(e) {
  var p, r, x;
  p = r = Math.floor(Math.random() * (e - 2)) + 1;
  x = [r, e - r];
  //document.write  (x[0] + ", " + x[1] + "  ")
  outlet (1, x);
  return x;
};

split = function(p) {
  var e1, e2;
  if (p.energy < 3) {
    return [p];
  } else {
    [e1, e2] = split_nrg(p.energy);
    if (p.type === "electron") {
     // document.write (p.type# + "(" + p.energy + ") splits into electron and photon <br/>")
      return [newParticle("electron", e1), newParticle("photon", e2)];
    }
    if (p.type === "photon") {
      p// document.write  (p.type + "(" + p.energy + ") splits into electron and positron  <br/>")
      return [newParticle("electron", e1), newParticle("positron", e2)];
    }
    // document.write  (p.type + "(" + p.energy + ") splits into positron and photon  <br/>")
    return [newParticle("positron", e1), newParticle("photon", e2)];
  }
};

maybe_split = function(p) {
  if (Math.random() < 0.2) {
    return split(p);
  }
  return [p];
};

merge = function(p1, p2) {
  if (p1.type === "photon") {
    return [p1, p2];
  }
  
  //document.write ("Merging " + p1.type + "(" + p1.energy + ") with (" + p2.type + "(" + p2.energy + ")<br/>") 
  if (p1.type === "electron") {
    if (p2.type === "electron") {
      return [p1, p2];
    }
    if (p2.type === "photon") {
      return [newParticle("electron", p1.energy + p2.energy)];
    }
    return [newParticle("photon", p1.energy + p2.energy)];
  }
  if (p2.type === "electron") {
    return [newParticle("photon", p1.energy + p2.energy)];
  }
  if (p2.type === "photon") {
    return [newParticle("positron", p1.energy + p2.energy)];
  }
  return [p1, p2];
};

shuffle = function(xs) {
  var currentIndex, randomIndex, temporaryValue;
  currentIndex = xs.length;
  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex = currentIndex - 1;
    temporaryValue = xs[currentIndex];
    xs[currentIndex] = xs[randomIndex];
    xs[randomIndex] = temporaryValue;
  }
  return xs;
};

maybe_merge = function(particles) {
  var new_parts, others, p1, p2;
  if (Object.keys(particles).length < 4) {
    return particles;
  }
  shuffle(particles);
  p1 = particles[0];
  p2 = particles[1];
  others = particles.slice(2);
  return new_parts = merge(p1, p2).concat(others);
};

flatten = function(array) {
  return array.reduce((function(x, y) {
    if (Array.isArray(y)) {
      return x.concat(flatten(y));
    } else {
      return x.concat(y);
    }
  }), []);
};

update_system = function(system) {
  var mm, ms, p;
  ms = flatten((function() {
    var j, len, ref, results;
    ref = system.particles;
    results = [];
    for (j = 0, len = ref.length; j < len; j++) {
      p = ref[j];
      results.push(maybe_split(p));
    }
    return results;
  })());
  mm = maybe_merge(ms);
  mm = maybe_merge(mm);
  return {
    particles: mm
  };
};

report = function(system) {
  var out, pp_part, red;
  pp_part = function(s, p) {
    //return s + p.type + " (" + p.energy + ")";
    messnamed("4feynman.pp_part", pp_part);
	messnamed("4feynman.energy", p.energy);
	messnamed("4feynman.ptype", p.type);
    return p.energy;
  };
  out = system.particles.reduce(pp_part, "");
  red = function(acc, cur) {
    return acc + cur.energy;
  };
  out = out + (system.particles.reduce(red, 0));
  messnamed("4feynman.out", out);
  return out;
};

the_system = {
  particles: [newParticle("electron", 100)]
};

run = function(runs) {
  var i, results;
  i = 0;
  results = [];
  while (i < runs) {
    //document.write ("Step " + i + " ");
    the_system = update_system(the_system);
    outlet(0,(report(the_system)));
    //console.log();
    results.push(i = i + 1);
  }
  return results;


};

